exports = module.exports = {
    student: {
        name: {
            type: String,
            required: true
        },
        roll: {
            type: String,
            required: true
        }
    },
    subject: {
        name: {
            type: String,
            required: true
        },
        id: {
            type: Number,
            required: true
        }
    },
    attendance: {
        subject_id: {
            type: Number,
            required: true
        },
        absentees: {
            type: [Number],
            default: [0],
            required: true
        },
        date: {
            type: Date,
            default: Date.now()
        }
    },
    notice: {
        title: {
            type: String,
            required: true
        },
        body: {
            type: String,
            required: true
        },
        date:{
            type: Date,
            default: Date.now()
        }
    }
}