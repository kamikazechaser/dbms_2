const restify = require("express-restify-mongoose")
const methodOverride = require("method-override");
const bodyParser = require("body-parser");
const mongoose = require("mongoose")
const express = require("express");
const path = require("path");

const config = require("./config");
const models = require("./models");

mongoose.connect(config.mongo);

const app = express();
const router = express.Router()

app.use(bodyParser.json());
app.use(methodOverride())
app.use(express.static(path.join(__dirname, "public")));

restify.serve(router, mongoose.model("student", new mongoose.Schema(models.student), "students"));
restify.serve(router, mongoose.model("subject", new mongoose.Schema(models.subject), "subjects"));
restify.serve(router, mongoose.model("attendance", new mongoose.Schema(models.attendance), "attendances"));
restify.serve(router, mongoose.model("notice", new mongoose.Schema(models.notice), "notices"));

app.get("/api/v1/report", (req, res) => {
  //const subjectModel = mongoose.model("subject", models.subject);
  //const attendanceModel = mongoose.model("attendance", models.attendance);

  //return subjectModel.find().exec((error, docs) => {
  // return res.status(200).json(docs);
  // });
});

app.use(router);

app.listen(config.port, () => {
  console.log("Express server listening on port 3000");
});